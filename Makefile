# ========================================================
# Database
# ========================================================
# SQAD v3
DATE=$(shell date '+%Y-%m-%d_%H_%M_%S')

database_v3: database/sqad_v3_$(DATE)
database_v3.2: database/sqad_v3.2_$(DATE)
database_full: database/sqad_full_wiki_$(DATE)

database/sqad_v3_%:
	$(MAKE) -C database sqad_v3_$(DATE)/

database/sqad_v3.2_%:
	$(MAKE) -C database sqad_v3.2_$(DATE)/

database/sqad_full_wiki_%:
	$(MAKE) -C database sqad_full_wiki_$(DATE)/

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# SQAD v2
database_v2: database/sqad_v2_$(DATE)

database/sqad_v2_%:
	mkdir $@
	$(MAKE) -C database sqad_v2_$(DATE)/

# ========================================================
# Evalution
# ========================================================
evaluate_weights:
	$(MAKE) -C evaluation evaluate_all_weights DATABASE=../database/sqad_v3_2022-12-20_11_59_24/
tables/weight_stats.tsv:
	./scripts/compute_percentages.py --res_dir evaluation/results > $@

# Odhad skutočnej úspešnosti:
# cat evaluation/results/docsel_n5_sqad3_w0.3.txt | ./scripts/get_csv_form_manual_check.py -s /nlp/projekty/sqad/sqad_v3/data/ -> correct 12417
#corr(12417) - `cat evaluation/results/docsel_n5_sqad3_w0.3.txt | ./scripts/get_csv_form_manual_check.py -s /nlp/projekty/sqad/sqad_v3/data/ | grep -v "FAIL" | grep "YES_NO" | wc -l` = 90.47  # odpočítané YES_NO lebo tam nie je answer extraction

tables/weight_stats_per_q_type.tsv:
	./scripts/compute_percentages_per_q_type.py --res_dir evaluation/results/ --dir /nlp/projekty/sqad/sqad_v3/ > $@
	
# ========================================================

# ========================================================
# Test example
# ========================================================
test:
	echo "Kdo nasal Létajícií Jaguár" | ./scripts/doc_selection.py --db_path database/sqad_v3_2020-01-31_14_17_02/ -n 3
