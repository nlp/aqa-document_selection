#! /usr/bin/python3.7
# coding: utf-8
import sqlite3
import os


class Database:
    def __init__(self, db_name):
        """
        Init database object
        :param db_name: database path
        """
        self.conn = self.connect_database(db_name)

    def connect_database(self, db_name):
        """
        Connection to database
        :param db_name: database path
        :return: obj, connection to SQLite database
        """
        if os.path.exists(db_name):
            conn = sqlite3.connect(db_name)
        else:
            conn = self.create_database(db_name)
        return conn

    @staticmethod
    def create_database(db_name):
        """
        Creates SQLite database. Two tables, one for corpus info that was used for creating, one for terms.
        :param db_name: database path
        :return: None
        """

        conn = sqlite3.connect(db_name)
        c = conn.cursor()
        c.execute('CREATE TABLE sqad (id INTEGER PRIMARY KEY, rid TEXT, url TEXT, text_lemma_normalized TEXT)')
        c.execute('CREATE INDEX url_idx ON sqad (url)')

        conn.commit()
        return conn

    def close(self):
        """
        Close database
        :return: None
        """
        self.conn.commit()
        self.conn.close()

    def insert_record(self, rid, url, text_lemma_normalized):
        """
        Inserting terms into table
        :param rid: str
        :param url: str
        :param text_lemma_normalized: str
        :param question_lemma: str
        :return: None
        """
        c = self.conn.cursor()
        c.execute('INSERT INTO sqad VALUES (?,?,?,?)', (rid, rid, url, text_lemma_normalized))

        # source_id, word,
        self.conn.commit()

    def selectall_text_lemma(self):
        """
        Selecting line matched by term
        :return: list, result form database
        """
        try:
            c = self.conn.cursor()
            c.execute('SELECT rid, text_lemma_normalized FROM sqad')
            return c.fetchall()

        except sqlite3.OperationalError:
            return []

    def select_url(self, url):
        """
        Selecting RID according URL
        :param url: str
        :return: list, result form database
        """
        try:
            c = self.conn.cursor()
            c.execute('SELECT rid FROM sqad WHERE url=?', (url,))
            return c.fetchall()

        except sqlite3.OperationalError:
            return []

    def selectall(self):
        """
        Selecting line matched by term
        :return: list, result form database
        """
        try:
            c = self.conn.cursor()
            c.execute('SELECT rid, url, text_lemma_normalized FROM sqad')
            return c.fetchall()

        except sqlite3.OperationalError:
            return []


def main():
    import argparse
    parser = argparse.ArgumentParser(description='Main print out only database content. For creating and '
                                                 'inserting use as module')
    parser.add_argument('-d', '--db_name', type=str,
                        required=True,
                        help='database name')

    args = parser.parse_args()

    db = Database(args.db_name)

    for rid, url, text_lemma_normalized in db.selectall():
        print('rid:{}\turl:{}\ntext:\t{}'.format(rid, url, text_lemma_normalized))
        print('==========================================================================================\n')
    print(len(db.selectall()))
    db.close()


if __name__ == "__main__":
    main()
