#! /usr/bin/env python3
# coding: utf-8
import json
import os
import re
import sys
from database import Database
from urllib.parse import unquote

struct = re.compile(r'^<[^>]+>$')
exclude_tag_re = re.compile(r'^(kI|k7|k8|k9|k0|k3).*')
rid_re = re.compile(r'\d{6}')
stopword_list_lemma = {"být", "mít", "chtít", "muset", "moct"}


def load_data(file_name):
    vert_lemma = []
    with open(file_name, 'r') as tf:
        for line in tf:
            line = line.strip()
            if not struct.match(line):
                _, lemma, tag = line.split('\t')[:3]
                if (not exclude_tag_re.match(tag)) and (lemma not in stopword_list_lemma):
                    vert_lemma.append(lemma)
    return vert_lemma


def create_database(db_name, dir_path, no_question):
    db = Database(f'{db_name}/sqlite.db')
    rid_per_same_doc = {}
    all_questions = []
    counter = 0
    for root, _, _ in os.walk(dir_path):
        rid = root.strip().split('/')[-1]
        if rid_re.match(rid):
            doc_lemma = load_data(os.path.join(root, "03text.vert"))

            with open(os.path.join(root, "04url.txt"), 'r') as uf:
                url = unquote(uf.readlines()[0].strip())

            if no_question:
                question_lemma = []
            else:
                question_lemma = load_data(os.path.join(root, "01question.vert"))
                all_questions.append((' '.join(question_lemma), url, rid))

            if rid_per_same_doc.get(url):
                rid_per_same_doc[url].append(rid)
            else:
                rid_per_same_doc[url] = [rid]

            if not db.select_url(url):
                sys.stderr.write(f'add ({counter}): {rid} ({url}): T {len(doc_lemma)}|Q {len(question_lemma)}\n')
                db.insert_record(rid, url, ' '.join(doc_lemma))
            else:
                sys.stderr.write(f'skipp ({counter}): {rid} ({url}) same doc\n')
            counter += 1

    # Storing same records with documents
    with open(f'{db_name}/record_with_same_documents.json', 'w') as f:
        json.dump(rid_per_same_doc, f)
    with open(f'{db_name}/all_questions.json', 'w') as f:
        json.dump(all_questions, f)

    db.close()


def main():
    import argparse
    parser = argparse.ArgumentParser(description='SQAD to sqlite database')
    parser.add_argument('-d', '--dir', type=str,
                        required=True,
                        help='SQAD directory')
    parser.add_argument('--db_name', type=str,
                        required=True,
                        help='Database name')
    parser.add_argument('--no_question', action='store_true',
                        required=False, default=False,
                        help='No questions add to db')
    args = parser.parse_args()

    create_database(args.db_name, args.dir, args.no_question)


if __name__ == "__main__":
    main()
