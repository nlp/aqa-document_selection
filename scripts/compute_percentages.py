#! /usr/bin/env python3
# coding: utf-8
import sys
import re
import os


def get_percentage(freq, total):
    """
    Calculate percentages
    :param freq: int, frequency
    :param total: int, total
    :return: int
    """
    return "{0:.2f}".format(100 * float(freq)/float(total))


def main():
    import argparse
    parser = argparse.ArgumentParser(description='Compute percetage distibution of correct documetn '
                                                 'elections prositions')
    parser.add_argument('-o', '--output', type=argparse.FileType('w'),
                        required=False, default=sys.stdout,
                        help='Output')
    parser.add_argument('-r', '--res_dir', type=str, required=True, help='Directory with all results')

    parser.add_argument('-c', '--combined_results', type=str,
                        required=False, default='0:4',
                        help='Combined result of positions: 0,1; 0,1,2; 0,1,2,3; 0,1,3,2,4')
    args = parser.parse_args()

    final_table = {}
    final_table_combined = {}

    for root, dirs, files in os.walk(args.res_dir):
        for file_name in files:
            if file_name.endswith(".txt"):
                weight = re.match('.*?w(\d+\.\d+)\.txt', file_name).group(1)
                score_dict = {}
                total = 0

                # =========================================================
                # Basic accuracy
                with open(os.path.join(root, file_name), 'r') as f:
                    for line in f:
                        if line.strip():
                            scores, position = line.strip().split('\t')
                            position = int(position)
                            try:
                                score_dict[position] += 1
                            except KeyError:
                                score_dict[position] = 1
                            total += 1

                for position, freq in score_dict.items():
                    final_table[(weight, '{}'.format(position + 1))] = get_percentage(freq, total)

                # =========================================================
                # Combined score
                _from, _to = args.combined_results.split(':')
                min_n = int(_from)
                max_n = int(_to)

                for upper_bound in range(min_n, max_n + 1):
                    combined_score = 0
                    for actual in range(min_n, max_n + 1):
                        if actual <= upper_bound:
                            combined_score += score_dict[actual]
                    final_table_combined[(weight, '1:{}'.format(upper_bound + 1))] = get_percentage(combined_score,
                                                                                                    total)

    # ========================================================================
    # Output table print
    position_sort = set()
    position_sort_combined = set()

    for _, pos in final_table.keys():
        position_sort.add(pos)

    for _, pos_com in final_table_combined.keys():
        position_sort_combined.add(pos_com)

    position_sort = sorted(position_sort)
    position_sort_combined = sorted(position_sort_combined)

    args.output.write('Weight/Position\t{}\n'.format('\t'.join([x if x != '0' else 'Fail' for x in position_sort] + position_sort_combined)))

    all_weights = set()

    for w, _ in final_table.keys():
        all_weights.add(w)

    for w in all_weights:
        args.output.write('{:.3f}'.format(float(w)))
        for pos in position_sort:
            try:
                args.output.write('\t{:.2f}'.format(float(final_table[w, pos])))
            except KeyError:
                args.output.write('\t')
        for pos_com in position_sort_combined:
            try:
                args.output.write('\t{:.2f}'.format(float(final_table_combined[w, pos_com])))
            except KeyError:
                args.output.write('\t')

        args.output.write('\n')


if __name__ == "__main__":
    main()
