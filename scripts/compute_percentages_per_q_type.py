#! /usr/bin/env python3
# coding: utf-8
import sys
import re
import os


def get_percentage(freq, total):
    """
    Calculate percentages
    :param freq: int, frequency
    :param total: int, total
    :return: int
    """
    return "{0:.2f}".format(100 * float(freq)/float(total))


def get_record_id(scores):
    """
    Find out record id from string
    :param scores: str
    :return: str
    """
    return re.search('\(\'(\d+)\',\s*\'\d+\',\s*\d+\.\d+\)', scores).group(1)


def get_question_type(r_id, directory, q_type_list):
    """
    Find out question type
    :param r_id: str, record id
    :param directory: str, sqad directory
    :param q_type_list: set, set of question types
    :return: str, question type
    """
    q_type = 'ERROR'
    with open('{}/data/{}/05metadata.txt'.format(directory, r_id), 'r') as mf:
        for line in mf:
            if re.match('<q_type>.*?</q_type>', line):
                q_type = re.match('<q_type>(.*?)</q_type>', line).group(1)

    q_type_list.add(q_type)
    return q_type


def get_weight_from_filename(filename):
    """
    Extract weight form filename
    :param filename: str, filename
    :return: float, weight
    example file name: docsel_n5_sqad3_w0.06.txt
    """
    return re.match('^.*?w(\d+\.\d+).*?$', filename).group(1)


def main():
    import argparse
    parser = argparse.ArgumentParser(description='Compute percetage distibution of correct documetn '
                                                 'elections prositions')

    parser.add_argument('-o', '--output', type=argparse.FileType('w'),
                        required=False, default=sys.stdout,
                        help='Output')
    parser.add_argument('-c', '--combined_results', type=str,
                        required=False, default='0:4',
                        help='Combined result of positions: 0,1; 0,1,2; 0,1,2,3; 0,1,3,2,4')
    parser.add_argument('--dir', type=str,
                        required=True, default=None,
                        help='Directory with records needed as parameter')
    parser.add_argument('-r', '--res_dir', type=str, required=True, help='Directory with all results')
    args = parser.parse_args()

    final_table = {}
    final_table_combined = {}

    for root, dirs, files in os.walk(args.res_dir):
        for file_name in files:
            if file_name.endswith(".txt"):
                score_per_q_type = {}
                q_type_list = set()
                total_per_q_type = {}
                weight_value = get_weight_from_filename(file_name)

                # =========================================================
                # Basic accuracy
                with open(os.path.join(root, file_name), 'r') as f:
                    for line in f:
                        if line.strip():
                            scores, position = line.strip().split('\t')
                            position = int(position)
                            r_id = "%06d" % int(get_record_id(scores))
                            q_type = get_question_type(r_id, args.dir, q_type_list)
                            try:
                                score_per_q_type[(q_type, position)] += 1
                            except KeyError:
                                score_per_q_type[(q_type, position)] = 1

                            try:
                                total_per_q_type[q_type] += 1
                            except KeyError:
                                total_per_q_type[q_type] = 1

                for (q_type, position), freq in score_per_q_type.items():
                    final_table[(q_type, weight_value, '{}'.format(position + 1))] = get_percentage(freq, total_per_q_type[q_type])

                # =========================================================
                # Combined score
                _from, _to = args.combined_results.split(':')
                min_n = int(_from)
                max_n = int(_to)

                for q_type in q_type_list:
                    for upper_bound in range(min_n, max_n + 1):
                        combined_score = 0
                        for actual in range(min_n, max_n + 1):
                            if actual <= upper_bound:
                                try:
                                    combined_score += score_per_q_type[(q_type, actual)]
                                except KeyError:
                                    pass
                        final_table_combined[(q_type, weight_value, '1:{}'.format(upper_bound + 1))] = get_percentage(combined_score, total_per_q_type[q_type])

        # ========================================================================
        # Print output table
        position_sort = set()
        position_sort_combined = set()
        all_weights = set()
        all_q_types = set()

        for _, _, pos in final_table.keys():
            position_sort.add(pos)

        for _, _, pos_com in final_table_combined.keys():
            position_sort_combined.add(pos_com)

        position_sort = sorted(position_sort)
        position_sort_combined = sorted(position_sort_combined)

        for _, w, _ in final_table.keys():
            all_weights.add(w)

        for q, _, _ in final_table.keys():
            all_q_types.add(q)

        for q in all_q_types:
            args.output.write('{}\n'.format(q))
            args.output.write('Weight/Position\t{}\n'.format(
                '\t'.join([x if x != '0' else 'Fail' for x in position_sort] + position_sort_combined)))
            for w in all_weights:
                args.output.write('{:.3f}'.format(float(w)))
                for pos in position_sort:
                    try:
                        args.output.write('\t{:.2f}'.format(float(final_table[q, w, pos])))
                    except KeyError:
                        args.output.write('\t')
                for pos_com in position_sort_combined:
                    try:
                        args.output.write('\t{:.2f}'.format(float(final_table_combined[q, w, pos_com])))
                    except KeyError:
                        args.output.write('\t')
                args.output.write('\n')


if __name__ == "__main__":
    main()
