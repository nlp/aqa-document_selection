#! /usr/bin/python3.7
# coding: utf-8
import sys
from doc_selection import compute_similarity_matrix
from doc_selection import transorm_output
from doc_selection import correct_sort
from database import connect_database
from database import select_question_lemma_id
import scipy
import numpy as np


def tf_norm(data_list):
    result = []
    max_item_idx = np.argmax(data_list)

    for idx, value in enumerate(data_list):
        if idx != max_item_idx:
            result.append(0.5 + (0.5*(value/data_list[max_item_idx])))
        else:
            result.append(0.5 + (0.5*(value/data_list[np.argmax(np.concatenate((data_list[0:idx], data_list[idx+1:])))])))

    return np.array(result)


def tf_norm_2(data_list):
    result = []

    for value in data_list:
        result.append(1 + scipy.log2(value))
    return np.array(result)


def tf_norm_3(data_list):
    result = []

    for value in data_list:
        result.append(float(value)/sum(data_list))
    return np.array(result)


def main():
    import argparse
    parser = argparse.ArgumentParser(description='Document selection')
    parser.add_argument('-o', '--output', type=argparse.FileType('w'),
                        required=False, default=sys.stdout,
                        help='Output')
    parser.add_argument('--db_path', type=str,
                        required=True,
                        help='DB name')
    parser.add_argument('--number', type=int,
                        required=False, default=5,
                        help='Top N best scored')
    parser.add_argument('--q_id', type=int,
                        required=True,
                        help='q_id')
    parser.add_argument('--tf_idf_weight', type=float,
                        required=False, default=1,
                        help='Weight of question word tf-idf on final score')
    parser.add_argument('-v', '--verbose', action='store_true',
                        required=False, default=False,
                        help='Verbose mode')
    args = parser.parse_args()

    conn = connect_database('{}/sqlite.db'.format(args.db_path))
    doc_id_list, dictionary, tfidf, similarity_matrix = compute_similarity_matrix(conn, args.db_path)
    dictionary[0]

    q_id, question_lemma = select_question_lemma_id(conn, args.q_id)[0]
    q_representation = [dictionary.doc2bow(question_lemma.split(' '))]


    sims, query_csc = similarity_matrix.get_similarities(tfidf[q_representation])

    top_n = sorted(enumerate(list(sims[0])), key=lambda x: x[1], reverse=True)

    top_n = correct_sort(top_n, query_csc, similarity_matrix, args.tf_idf_weight)

    top_n_transformed = transorm_output(q_id, top_n, doc_id_list, args.number)

    if args.verbose:
        print('q_representation: {}'.format(q_representation))
        print('------------------------------------')
        print('sims: {}'.format(top_n[:args.number]))
        print('sims_transformed: {}'.format(top_n_transformed))

    for idx, (doc_id, score) in enumerate(top_n[:args.number]):
        print('{}============ {} ({}): {} =================='.format(idx +1, doc_id,
                                                                   top_n_transformed[idx][1],
                                                                   top_n_transformed[idx][2]))

        # query_csc.nonzero is transformation to sparse matrix
        # (array([ 199,  351, 1093, 2114, 2236], dtype=int32), array([0, 0, 0, 0, 0], dtype=int32))
        # to access index id of words
        for i in query_csc.nonzero()[0]:
            print('{} -> {}'.format(dictionary.id2token[i],
                                    similarity_matrix.index[(doc_id, i)]))
            if args.verbose:
                print('q: {} -> {}'.format(dictionary.id2token[i], query_csc[(i, 0)]))
                print('------------------------------------')


if __name__ == "__main__":
    main()


"""
první -> 0.020173627883195877
český -> 0.035521768033504486
stát -> 0.03459390252828598
prezident -> 0.07970655709505081
republika -> 0.04200121760368347

"""
