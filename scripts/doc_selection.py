#! /usr/bin/env python3
# coding: utf-8
# Old document selection created according original implementation from aqa git bce9819
import json
import sys
import os
from database import Database
from gensim_git.gensim.corpora.dictionary import Dictionary
from gensim_git.gensim.corpora.mmcorpus import MmCorpus
from gensim_git.gensim.models.tfidfmodel import TfidfModel
from gensim_git.gensim.similarities.docsim import SparseMatrixSimilarity


class DocumentSelection:
    def __init__(self, db_path, tf_idf_weight):
        """
        :param db_path: str, database name
        :param tf_idf_weight: float, weight of word tf-idf
        """

        self.db_path = db_path
        self.tf_idf_weight = tf_idf_weight
        self.db = Database('{}/sqlite.db'.format(self.db_path))

        self.records_with_same_documents = {}
        try:
            with open('{}/record_with_same_documents.json'.format(self.db_path), 'r') as f:
                self.records_with_same_documents = json.load(f)
        except FileNotFoundError:
            sys.stderr.write('Not exists {}/record_with_same_documents.json'.format(self.db_path))
            pass

        self.all_questions = []
        try:
            with open('{}/all_questions.json'.format(self.db_path), 'r') as f2:
                self.all_questions = json.load(f2)
        except FileNotFoundError:
            sys.stderr.write('Not exists {}/all_questions.json'.format(self.db_path))
            pass

        self.doc_id_list, self.dictionary, self.tfidf, self.similarity_matrix = self.compute_similarity_matrix()

    def transorm_output(self, q_id, top_n, number):
        """
        Transform output: id from similarity matrix into id of document on sqad database
        :param q_id:
        :param top_n:
        :param number:
        :return:
        """
        result = []
        for n, score in top_n:
            result.append((q_id, self.doc_id_list[n], score))

        return result[:number]

    @staticmethod
    def eval_doc_selection(top_n, records_by_url, url):
        """
        Find out if doc ID is same as question ID
        :param top_n: list, top n scored documents
        :param records_by_url:
        :param url:
        """
        same_pos = -1
        for idx, (q_id, d_id, _) in enumerate(top_n):
            if q_id == d_id:
                same_pos = idx
                break
            try:
                if d_id in records_by_url[url]:
                    same_pos = idx
                    break
            except KeyError:
                pass

        return same_pos

    def compute_similarity_matrix(self):
        """
        Compute or load gensim dict and similarity matrix
        :return:
        """
        if os.path.isfile('{}/gensim.dict'.format(self.db_path)) and \
                os.path.isfile('{}/gensim.coprora'.format(self.db_path)):

            doc_id_list = []
            for doc_id, doc_lemma in self.db.selectall_text_lemma():
                if doc_lemma:
                    doc_id_list.append(doc_id)

            dictionary = Dictionary.load('{}/gensim.dict'.format(self.db_path))
            corpus = MmCorpus('{}/gensim.coprora'.format(self.db_path))
            tfidf = TfidfModel(corpus)
            similarity_matrix = SparseMatrixSimilarity(tfidf[corpus], num_features=len(dictionary))

        else:
            documents = []
            doc_id_list = []
            sys.stderr.write('Loading sqlite database\n')
            for doc_id, doc_lemma in self.db.selectall_text_lemma():
                if doc_lemma:
                    doc_id_list.append(doc_id)
                    documents.append(doc_lemma.split(' '))

            """TF-IDF"""
            sys.stderr.write('Computing similarity matrix\n')
            dictionary = Dictionary(documents)
            dictionary.save('{}/gensim.dict'.format(self.db_path))

            corpus = [dictionary.doc2bow(text) for text in documents]
            MmCorpus.serialize('{}/gensim.coprora'.format(self.db_path), corpus)
            tfidf = TfidfModel(corpus)
            similarity_matrix = SparseMatrixSimilarity(tfidf[corpus], num_features=len(dictionary))

        return doc_id_list, dictionary, tfidf, similarity_matrix

    def correct_sort(self, top_n, query_csc):
        """
        Correct score of document according appearance of word from question in document and tf_idf_weight parameter
        :param top_n: top_n scored documents
        :param query_csc: question matrix to determine tf-idf of question words
        :return:
        """
        new_top_n = []
        for doc_id, score in top_n:
            non_zero_tokens = 0
            for i in query_csc.nonzero()[0]:
                if self.similarity_matrix.index[(doc_id, i)] > 0:
                    non_zero_tokens += query_csc[(i, 0)] * self.tf_idf_weight
            new_top_n.append((doc_id, score + non_zero_tokens))

        return sorted(new_top_n, key=lambda x: x[1], reverse=True)


    def doc_selection(self, input_data, number, evaluate=False, only_tf_idf=False):
        """
        Document selection according to TF-IDF
        :param input_data: input question
        :param number: int, number of documents in results, sorted by sore
        :param evaluate: bool, print evaluation
        :param only_tf_idf: bool, only create similarity matrix
        :return: (top_n, position within top_n if same -1 otherwise)
        """
        same_pos = ''

        if not only_tf_idf:
            if input_data:
                question_content = input_data.strip().split(' ')

                q_representation = [self.dictionary.doc2bow(question_content)]

                sims, query_csc = self.similarity_matrix.get_similarities(self.tfidf[q_representation])
                top_n = self.correct_sort(enumerate(list(sims[0])), query_csc)
                top_n = self.transorm_output(-1, top_n, number)

                yield top_n, same_pos

            else:
                for question_lemma, url, q_id in self.all_questions:
                    q_representation = [self.dictionary.doc2bow(question_lemma.split(' '))]

                    sims, query_csc = self.similarity_matrix.get_similarities(self.tfidf[q_representation])
                    top_n = self.correct_sort(enumerate(list(sims[0])), query_csc)  # TF-IDF weight
                    top_n = self.transorm_output(q_id, top_n, number)

                    # Evaluate if question ID is same as document ID
                    if evaluate:
                        same_pos = self.eval_doc_selection(top_n, self.records_with_same_documents, url)

                    yield top_n, same_pos


def main():
    import argparse
    parser = argparse.ArgumentParser(description='Document selection')
    parser.add_argument('-o', '--output', type=argparse.FileType('w'),
                        required=False, default=sys.stdout,
                        help='Output')
    parser.add_argument('-i', '--input', type=argparse.FileType('r'),
                        required=False, default=sys.stdin,
                        help='Input')
    parser.add_argument('-n', '--number', type=int,
                        required=False, default=5,
                        help='Top N best scored')
    parser.add_argument('--db_path', type=str,
                        required=True,
                        help='Top N best scored')
    parser.add_argument('--evaluate', action='store_true',
                        required=False, default=False,
                        help='Evaluate document selection')
    parser.add_argument('--tf_idf_weight', type=float,
                        required=False, default=0.26,
                        help='Weight of question word tf-idf on final score')
    parser.add_argument('--only_tf_idf', action='store_true',
                        required=False, default=False,
                        help='Create only Gensim similarity matrix')
    args = parser.parse_args()

    if not os.isatty(args.input.fileno()):
        input_question = args.input.read()
    else:
        input_question = ''

    ds = DocumentSelection(args.db_path, args.tf_idf_weight)
    for top_n, same_pos in ds.doc_selection(input_question, args.number, args.evaluate, args.only_tf_idf):
        args.output.write('{}{}\n'.format(top_n, '\t{}'.format(same_pos) if same_pos != '' else ''))


if __name__ == "__main__":
    main()
